$fn = 100;
size = 10;
height=5;
chain=1;
hull(){
   translate([size,size,0]) cylinder(r=size,h=height);
     translate([0,-size*(7/5),0])rotate ([0,0,45]) cube([size,size,height]);
}
hull(){
   translate([-size,size,0]) cylinder(r=size,h=height);
     translate([0,-size*(7/5),0])rotate ([0,0,45]) cube([size,size,height]);
}
if(chain==1){
difference(){
    hull(){
    translate([-size*(1/10),size*(7/5),height/2])rotate ([0,90,0]) cylinder(d=height,h=size*(1/5));
        translate([-size*(1/10),size*(8/5),height/2])rotate ([0,90,0]) cylinder(d=height,h=size*(1/5));
    }
        hull(){
   translate([-size*(1/5),size*(7/5),height/2])rotate ([0,90,0]) cylinder(d=height*(3/4),h=size*(2/5));
            translate([-size*(1/5),size*(8/5),height/2])rotate ([0,90,0]) cylinder(d=height*(3/4),h=size*(2/5));
        }
}
}