$fn=64;
length=2;
width=2;
cylinder_size=5;
click=0;
module clicker (){
difference(){
    translate([0,0,2])cylinder(d=3,h=1.5);
    translate([0,0,2.5])cube([1,5,1],center=true);
}
difference(){
    translate([0,0,-2])cylinder(d1=3.4,d2=5,h=5);
    translate([0,0,1])cube([1,5,6],center=true);
    difference(){
        translate([0,0,-1.2])cylinder(d=5,h=3.5);
        translate([0,0,-2])cylinder(d=3,h=5);
    }
}
}
cube([8*length-0.2,8*width-0.2,3.2]);
for(j = [4:8:(8*length)]){
    for(i = [4:8:(8*width)]){
translate([j,i,0])cylinder(d=cylinder_size,h=1.7+3.2);
    }
}
if (click==1){
for(j = [4:8*length-8:(8*length)]){
    for(i = [4:8*width-8:(8*width)]){
translate([j,i,-3])clicker();
    }
}
}