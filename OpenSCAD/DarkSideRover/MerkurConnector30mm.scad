$fn=60;
rotate([90,0,0])
difference() {
    translate([0,1.3,0]) difference() {
        union() {
            cylinder(h=30,d=3.9);
            cylinder(h=1,d=4.2);
            translate([0, 0, 30-1]) cylinder(h=1,d=4.2);
            translate([0, 0, 30-4]) cylinder(h=1,d=4.2);
            translate([0, 0, 3]) cylinder(h=1,d=4.2);
        }
        cube([1.5,5,6], center=true);
        translate([0,0,31]) cube([1.5,5,8], center=true);
    }
    rotate([90,0,0]) translate([-10, 0,0]) cube([30,30,3]);
}