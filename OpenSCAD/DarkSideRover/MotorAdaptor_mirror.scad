include <ConnectorPin.scad>;
$fn=60;
plateWidth = 52;
plateHeight = 24;
connectorOffset = 6;
holeSpacing = 17.3;
holeOffset = 27;
mirror(v=[1,0,0]){
difference() {
    union() {
        cube([plateWidth, plateHeight, 2]);
        translate([connectorOffset +  0, 7, 1.3]) connectorPin();
        translate([connectorOffset + 10, 7, 1.3]) connectorPin();
        translate([connectorOffset + 20, 7, 1.3]) connectorPin();
    }
    translate([holeOffset, 16, -1]) cylinder(4, r=1.5);
    translate([holeOffset + holeSpacing, 16, -1]) cylinder(4, r=1.5);
}
}