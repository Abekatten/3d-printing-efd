include <ConnectorPin.scad>
$fn=64;

connectorPin([0,0,8]);
connectorPin([0,20,8]);
connectorPin([80,0,2]);
connectorPin([80,10,2]);
connectorPin([80,20,2]);
translate([-2.5,-2.5,0]) {
    cube([85, 25, 2.2]);
    cube([5,25,6]);
    cube([5,5,8.1]);
    translate([0,20,0]) cube([5,5,8.1]);
}