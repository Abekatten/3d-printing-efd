include <ConnectorPinDimensions.scad>
include <ConnectorPin.scad>

$fn = 60;
difference() {
    translate([0, 0, pinDiameter/2 -0.1]) rotate([90, 0, 0]) union() {
        connectorPin([0,0,0.1]);
        rotate([0,180,0]) connectorPin([0,0,0.1]);
        translate([0,0,-0.3]) cylinder(h=0.6, d=pinDiameter+1.0);
    }
    translate([0,0,-10]) cube([20,20,20], center=true);
}

