include <ConnectorPinDimensions.scad>

module connectorPin() {
    difference() {
        union() {
            cylinder(d=pinDiameter, h=pinHeight, $fn=60);
            translate([0,0,pinHeight-1]) cylinder(d=pinDiameter + 0.3,h=1, $fn=60);
        }
        translate([0,0,pinHeight/2+2]) cube([0.75, 4, pinHeight],center=true);
    }
}
